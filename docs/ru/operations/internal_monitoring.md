# Мониторинг Glaber сервера и прокси

Из соображений наглядности и удобства сделан синоним для внутренних метрик internal[...]

Внутренние счетчики отдаются группой одним запросом и предполагается, что будут интерпретированы и разбиты на метрики в препроцессинге. Такой подход позволяет проще расширять набор метрик. Для поддержки новых значений достаточно в препроцессинге добавить новые аттрибуты.

## internal[preprocessing]

```	
    {"queue_size":"0", "free":"97.99", "sent":"476322145"}
```
    `queue_size` - размер очереди, ожидающей обработки. Это метрики которые были отправленны поллерами и еще не приняты для внутренней обработки препроцесснгом. 
    `free` - количество свободных слотов метрик в процентах
    `sent` - счетчик обработанных на стороне препроцессинга метрик. Предполагается что будет использован для измерения производительности, скорости транзита метрик


## internal[processing]
    Набор метрик аналогичен препроцессингу. 

## internal[proc_stat]
     Возвращает метрики основных процессов. 
```
    [
        {"procid":"lld_manager_1","heap_usage":"5365760","process_name":"lld manager","process_num":"1","server_num":"8"},{"procid":"glb_snmp_poller_1","heap_usage":"8118272","process_name":"glb_snmp_poller","process_num":"1","server_num":"33"},{"procid":"timer_1","heap_usage":"9560064","process_name":"timer","process_num":"1","server_num":"12"},
        {"procid":"poller_10","heap_usage":"8118272","process_name":"poller","process_num":"10","server_num":"32"},
        {"procid":"proxy_poller_1","heap_usage":"9953280","process_name":"proxy poller","process_num":"1","server_num":"20"},{"procid":"poller_9","heap_usage":"8118272","process_name":"poller","process_num":"9","server_num":"31"},
        {"procid":"poller_5","heap_usage":"10084352","process_name":"poller","process_num":"5","server_num":"27"},
        {"procid":"discoverer_1","heap_usage":"9822208","process_name":"discoverer","process_num":"1","server_num":"14"},
        ....
    ]
```

### Параметры - идентификаторы:

`procid` - идентификатор имени процесса, состоит и имени и номера, пробелы заменяются символами подчеркивания
    
`process_name` - имя процесса
    
`process_num` - пордяковый номер процесса в группе одинаковых процессов
    
`server_num` - порядковый номер процесса среди всех процессов
    
### Параметры процесса
    
`heap_usage` - использование HEAP(RSS) памяти процессом. Не включает данные в различных кешах в разделяемой памяти (Shared memory)

### Шаблон, визуализация

Для удобства представления и просмотра метрик процессов, можно воспользоваться шаблоном:
    
Шаблон [glaber_process_monitoring.yaml](./glaber_process_monitoring.yaml)

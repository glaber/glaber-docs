# Бэкап и восстановление

Данные в Glaber хранятся в двух разных базах:
    - в MySQL или PostgreSQL - конфигурация и объекты системы, логи, события
    - в timeseries хранилище - собранные метрики. Как привило в качестве такого хранилища используется ClickHouse.

## Бекап и восстановление данных в SQL

### Mysql/Mariadb

Подробно процедуры бекапа и восстановления описаны на [https://mariadb.com/kb/en/backup-and-restore-overview/](https://mariadb.com/kb/en/backup-and-restore-overview/).

#### Бекап
Выполните команду (замените \<db_user\> и \<db_pass\> на имя пользователя и его пароль, имеющего доступ к БД glaber)
```
mysqldump -u<db_user> -p<db_pass> -E --single-transaction glaber > glaber.dump;
```
Эта команда сделает дамп базы данных glaber в файл ```glaber.dump```, который нужно будет использовать в случае необходимости восстановить базу


#### Восстановление
Для восстановления нужно сначала создать пустую базу. Если база уже сущесвует, то сначала ее необходимо удалить. 

Если база используется и есть активный процесс glaber сервера, то перед тем как удалять базу, необходимо остановить сервер.

Используйте SQL команды 
```
mysql> DROP DATABASE glaber;
mysql> CREATE DATABASE glaber;
``` 

После создания базы, на нее также нужно выдать права пользователю, или пользователям, от имени которых будет работать сервер и фронтенд:

Если пользователь не был заведен заранее, то создать его и задать пароль можно с помощью следующих команд: 
```
mysql> CREATE USER '<db_user>'@'localhost' IDENTIFIED BY '<db_pass>';
```
И выдать пользователю права на базу

```
mysql> GRANT ALL PRIVILEGES ON glaber.* TO '<db_user>'@'localhost';
```
После чего через утилиту mysql залить бекап в новую базу: 
```
mysql -u<db_user> -p<db_pass> glaber < glaber.dump
```

### Postgres SQL

Подробно процедуры бекапа и восстановления описаны на [https://www.postgresql.org/docs/current/backup.html](https://www.postgresql.org/docs/current/backup.html)

#### Бекап
Выполните команду (замените \<db_user\> и \<db_pass\> на имя кользователя и его пароль, имеющего доступ к БД glaber)

```
/usr/bin/pg_dump --dbname=postgresql://<db_user>:<db_pass>@localhost:5432/$db  > glaber.dump;
```
Эта команда сделает дамп базы данных glaber в файл ```glaber.dump```, который нужно будет использовать в случае необходимости восстановить базу

#### Восстановление
Авторизуйтесь под пользователем postgres и запустите утилиту ```psql```
```
sudo su postgres
psql
```

Для восстановления нужно сначала создать пустую базу. Если база уже сущесвует, то сначала ее необходимо удалить. 

Используйте SQL команды 
```
postgres=# DROP DATABASE glaber;
postgres=# CREATE DATABASE glaber;
``` 

После создания базы на нее также нужно выдать права пользователю, или пользователям, от имени которых будет работать сервер и фронтенд:

Если пользователь не был заведен заранее, то создать его и задать пароль: 
```
postgres=# CREATE USER <db_user> WITH PASSWORD '<db_pass>';

```
И выдать пользователю права на базу

```
postgres=# GRANT ALL PRIVILEGES ON DATABASE glaber TO <db_user>;
```
Завершите работу с psql с помощью команды ```exit```
После чего из под попльзователя postgres через утилиту ```psql``` залейте бекап в новую базу: 
```
psql glaber < glaber.dump
```

## Бекап и восстановление ClickHouse
Glaber может работать с различными timeseries хранилищами. У каждог из них есть свои утилиты и методы выгрузки\бекапа данных. Clickhouse в настоящий момент - самая популярная и удобная система для Glaber.

Стоит учитывать, что объемы данных могут быть очень большими, поэтому зачастую нужно реализовывать миграцию данных по частям (по партициям, по таблицам)

Процедуры бекапа и восстановления в Clickhouse описаны на: [https://clickhouse.com/docs/en/operations/backup/](https://clickhouse.com/docs/en/operations/backup/)


#### Бекап
Настройте, путь куда сервер будет бекапить данные. Для этого нужно создать файл **/etc/clickhouse-server/config.d/backup_disk.xml**
с содержимым, указывающим место в файловой системе, куда будут попадать бекапы (в данном случае **/backups/**):
```
<clickhouse>
    <storage_configuration>
        <disks>
            <backups>
                <type>local</type>
                <path>/backups/</path>
            </backups>
        </disks>
    </storage_configuration>
    <backups>
        <allowed_disk>backups</allowed_disk>
        <allowed_path>/backups/</allowed_path>
    </backups>
</clickhouse>
```
Подключитесь к Clickhouse. В самом простом случае, подключиться к серверу можно командой ```clickhouse client``` выполнив её локально на той же машине на которой запущен clickhouse.
Далее выполните бекап командой 
```
BACKUP DATABASE glaber TO Disk('backups', 'glaber.zip')
```

#### Восстановление
Настройте путь для работы с бекапами аналогично на сервере, на котором будут восстанавливаться данные, скопируйте в этот путь файл с бекапом и восстановите базу предварительно удалив существующую в случае её наличия:

```
DROP DATABASE glaber
RESTORE DATABASE glaber FROM Disk('backups', 'glaber.zip')
```


# Syslog: Мониторинг домофонных панелей
## Описание кейса

Необходимо организовать мониторинг домофонных панелей. Ориентировочное количество панелей в мониторинге несколько десятков тысяч

![Alt text](images/sokol.png)

Домофонные панели активно передают информацию в syslog. Поток syslog на сервере сбора логово отправляем на сервер мониторинга с помощью возможностей rsyslog. 

![Alt text](images/redirect_syslog.png)

В данном случае не получается организовать отправку с сохранением адресов источников, поэтому корректная адресная информация в логе отсутствует.

 ##  Описание решения
Создаем в мониторинге узел, который будет отвечать за приемку траффика syslog для домофонов, а также выполнять действия на всем объеме траффика:

* Подсчет суммарных метрик

* Перенаправление траффика панелей в связанные с ними узлы

* Автоматическое создание новых объектов узлов для домофонов, которые отсутствуют в мониторинге

## Настройка приема сислога
Для приема будем использовать воркер glb_syslog_worker, чтобы не смешивать существенный траффик логов с другими логами, перенаправим поток логов на альтернативный порт 1514, и сконфигурируем метрику для приема логов:

![Alt text](images/syslog_worker.png)

 Тип метрики - Server worker, в пути до исполняемого файла - воркера укажем, что слушать траффик нужно на альтернативном порту 1514. Сохранять историю не потребуется, сохранять будем на узлах - приемниках syslog траффика.

Проверка успешности: после настройки дождаться перечитыванием сервером конфигурации, после чего перейти в режим редактирования айтема и открыть вкладу "operational state", там будут видны приходящии логи.

## Подсчет метрик 
Для получения общих цифр по динамике услуги домофонии посчитаем цифры - статистику по источникам открытия дверей, и по успешности дозвонов в квартиры. 

Для каждого типа счетчика сделаем зависимую метрику от основной. Далее с помощью препроцессинга

![Alt text](images/agg_preproc.png)

1. отфильтруем только те сообщения, которые нам интересны. Обратите внимание, что в случае, если сообщение не представляет интереса, то мы выставляем кастомное значение NaN для корректной работы агрегации.

2. Агрегируем с типом count (подсчет количества метрик) с интервалом 60 секунд. В результате правило будет раз в 60 секунд отправлять счетчик пришедших в него айтемов 

## Перенаправление траффика

Панели идентифицируются Мак-адресом, который приходит в параметре  proc_id, поэтому будем создавать индивидуальные узлы для панелей с именем равным мак адресу, а для перенаправления будем использовать поиск имени узла по значению параметра  proc_id:

![Alt text](images/redirect_preproc.png)

### Контроль отсутствующих устройств

Если препроцессингу удается найти узел-приемник и метрику, то выполнение правил прекращается на правиле редиректа. 

Если узла или метрики на узле не нашлось, то выполнение правило продолжится, поэтому добавив правило агрегации, получим информацию - сколько логов от неизвестных панелей приходили за минуту. По этой метрике можно будет судить и сигнализировать о полноте заведения домофонов в системе.

![Alt text](images/unknown_panels.png) 

Пример - график показывает старт пустой системы, когда панели не заведены и по мере автоматического домофонных панелей количество неизвестных логов падает до нуля. 

Метрика также полезна тем, что может контроллировать наличие логов в которых отсутсвует информация для идентификации и перенаправления на узел.

## Автоматическое заведение новых панелей
Для того, чтобы автоматически обнаруживать новые устройства, существует специальное правило, позволяющее подготавливать на основе приходящих JSON объектов. Правило решает задачу фильтрации уникальных данных и снижения нагрузки на систему Discovery по созданию и проверке новых метрик. 

При получении новых уникальных данных правило пропускает эти данные, таким образом мгновенно создается новое устройство, если данные повторяются, то они пропускаются спустя интервал подтверждения наличия траффика, чтобы периодически продлевать время жизни автоматически созданных объектов. 

Важно: в правило нужно подавать только уникальную информацию, необходимую для создания новых объектов, поэтому предварительно целесообразно использовать правило фильтрации JSON полей, оставляя только те поля, которые нужны для создания объектов. Обязательно необходимо отфильтровать все высоко-вариативные данные, такие как время, значения, логи, счетчики. В нашем случае нам важен только физический адрес домофона, поэтому оставляем только поле proc_id:

![Alt text](images/auto_discovery.png)

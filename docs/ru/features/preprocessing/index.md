# Шаги препроцессинга

## [Перенаправление](./dispatching.md)
## [Аггрегация](./aggregation.md)

## preprocessing: Items dispatching

Items dispatching allows to "redirect" item to another host and item. 

This is typically required for data coming from Server workers: [Server workers](features/workers.md).

Items dispatching takes two params: field name for host value  and metric name:
![](images/2022-08-25-12-07-58.png)

1. First param is used to find a host name in incoming JSON data. Value might be in several formats:

    Plain text:
   ~~~
   proc_id
   ~~~
    JSON path identification:
   ~~~
   $.proc_id
   ~~~
   
   
 Also field supports prefixed/suffixed notation, then field name should be enclosed in brackets **{}**:
   ~~~
   metric.{proc_id}.name.test.com
   ~~~
This allows very basic data field conversion done in-House. Only the first field name in brackets is processed.

Incoming JSON will be search for the field with name 'proc_id' and it's value will be used to do a host lookup. Host name is searched by 'Host' value, not 'Name'. Also search is case-sensitive.

If incoming data has host information in first level fields, use plain text, if the information is inside other objects or array, utilize the power of JSON path syntax:

Example: 
~~~
{ 
    ...
    "host":nil,
    "sd": {
        "hostinfo":"my-server-host",
        "user":"#234524352",
        ....
    }
    ....
}
~~~
in such a case to send the metric to the host 'my_server_host' use JSON path:
~~~
$.sd.hostinfo
~~~

If there is a need to postfix the server name, for example, add domain, use something like:

~~~
{$.sd.hostinfo}.user-server.test.loc
~~~
to search and route metric to host
~~~
my-server-host.user-server.test.loc
~~~



2. The second param has a static text value - item name.  From the previous example the result of the rule will be search of host 'my-server-host.user-server.test.loc', then metric with name 'log' is searched, if found, then item is send to preprocessing again for the new item.


### Further rule processing

Rule has **inverse** fail logic - if a metric has been redirected, it returns FAIL to stop firther processing. 

If a **host:metric** hasn't been found or other errors had happen, rule successes and processing continues, allowing to try another redirects or other kind of processing for non-redirected metrics: 

For example you might want to setup an [Aggregation count rule](aggregation.md) to count for non redirected metrics and setup a trigger to signal that there are a lot of data coming for unknown hosts.



## Локальное перенаправление метрик

### Задача, мотивация

Существуют ситуации, когда значение метрики нужно поместить (перенаправить) в одну из большого количества метрик. Если метрик для перенаправления небольшое количество, то можно воспользоваться зависимыми метриками, однако если их существенное количество, то требуется большое количество операций: в каждой зависимой метрике будет произведена операция сравнения и фильтрации, что неэффективно, пример: требуется посчитать байты по диапазонам ip адресов.


### Логика работы

Пример входных данных

```
{ "ip_net":"10.0.2.0/24" , ... , "bytes":"243525" }
```

Для каждой сети существует метрика - счетчик байт: `in_bytes[10.0.2.0/24]`, очевидно, что таких метрик будут тысячи и реализация через зависимые метрики будет слишком затратной.


В таком случае можно использовать локальное перенаправление: 
в качестве параметра перенаправления указать строковую конструкцию, которая покажет, каким образом сформировать имя метрики для поиска для поиска имена метрики: 


local_dispatch: `in_bytes[{$.ip_net}]`  (поддерживаются Json path операции) или `in_bytes[{ip_net}]` (указано имя объекта, должен быть объектом первого уровня)


В таком случае из примера выше будет получено значение поля с ключом `ip_net`, а значение поля будет подставлено в шаблон имени метрики и данные будут отправлены в полученную метрику и обработка шагов препроцессинга прекратится. Если такой метрики не существуют, то продолжится обработка шагов препроцессинга дальне.


## Фильтрация json

Позволяет отфильтровать и оставить в json только указанные объекты

### Пример

Входящий json:

```
{"param_a":"123", "param_b":"245", "param_c":"897", "param_d":"1020"}
```

параметры фильтрации: 

`param_a,param_d,param_z`

Результат (в запросе оставлены только присутствующие `param_a` и `param_d`,  `param_z` во входящем json отсутствует)

```
{"param_a":"123", "param_d":"1020"}
```

## Аггрегация  JSON данных для обнаружения

### Задача, мотивация

Для ситуаций, когда требуется автоматическое обнаружение и создание новых метрик на основе входящего потока данных. Текущая реализация системы  LLD не позволяет подавать на ее вход поток существенных данных. 


### Логика работы

Шаг препроцессинга принимает и аккумулирует уникальные данные. Новые уникальные данные проходят шаг с минимальной задержкой, но при повторном приходе данных они задерживаются на указанный в параметрах шага промежуток времени. И через указанный промежуток посылается одна копия каждого уникального экземпляра данных, независимо от того, сколько одинаковых данных пришло.

Такая логика позволяет быстро создавать метрики, когда система видит их первый раз, и в то-же время при повторном появлении данных - не нагружать ими LLD систему. 

!!! Примечание 

    Время фильтрации и задержки должно быть меньше, чем время удаления данных в настройках Discovery данных.
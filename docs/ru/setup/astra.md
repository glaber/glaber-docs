# Инструкция по установке Glaber на AstraLinux 1.7.x

## Общая информация

Установка Glaber на Debian10 и AstraLinux не отличается, используются одни и те-же пакеты из линейки Debian Buster (Debian 10).

В зависимости от версии AstraLinux, есть различные варианты установки фронтенда. 

В AstraLinux 1.7.3 поставляется php версии 7.3, для работы Glaber необходимо использовать версию 7.4 или выше. 

В качестве решения можно использовать php версии 8.2, от версии AstraLinux 1.7.5, либо запустить специально подготовленную версию php в докер - контейнере. Контейнер можно скачать по адресу: https://glaber.io/repo/misc/glaber-php-fpm-docker.gz

### Настройка и особенности контейнера: 
   
php-fpm в контейнере настроен для работы в chroot режиме, поэтому не будут работать симоволические ссылки. Поэтому файл конфигурации фронтенда zabbix.conf.php необходимо поместить в /usr/share/zabbix/conf/ непосредственно или с помощью "жесткой" ссылки.

php-fpm слушает на TCP порту (а не на Unix Domian сокете), это необходимо учесть в конфигурации nginx

php-fpm работает в chroot окружении, абсолютные пути до скриптов в параметрах fast cgi должны указываться относительно /usr/share/zabbix


##	Установка сервера

Устанавливаем необходимые утилиты и библиотеки: 
Для автоматической установки зависимостей нужно раскоментировать официальные репозитрии астры в /etc/apt/sources.list

После чего установить следующие пакеты, их нет на официальном DVD, но есть в официальных репозитариях
```
apt install libopenipmi0
apt install fping
apt install libssh-4
```

## Работа с snmp v3, поддержка сильного шифрования 



## Установка приложения - сервера

При наличии доступа в интернет пакеты устанавливать можно из репозитариев напрямую через пакетные менеджеры. Настройка автоматического обновления из репозитариев описана хтут

Примечание: данные способ установки подразумевает, что отсутствует возможность автоматической установки пакетов из репозиториев, например по требованиям информационной безопасности или при работе системы в закрытых контурах

```
dpkg -i glaber-server-pgsql_x.у.z-1+debian10_amd64.deb
```
версия x.y.z меняется


##	Настройка и инициализациия базы данных

```
apt install postgresql
```

Имеющаяся в дистрибутиве AstraLinux 11 версия будет нормально работать с Glaber. 

стартуем сервер БД, добавляем в автозагрузку: 

```
systemctl start postgresql
systemctl enable postgresql
``` 

Создаем пользователя БД, инициализируем БД начальными данными, БД будет называться glaber, имя ползователя для БД используем zabbix:
```
sudo -u postgres createuser --pwprompt zabbix
```
вводим пароль, запоминаем, создаем базу glaber у который владелец будет пользователь zabbix 

```
sudo -u postgres createdb -O zabbix glaber
```
SQL – бекап начальной базы для инициализации был устновлен вместе с сервером и находится в каталоге 
```
/usr/share/doc/zabbix-server-pgsql/create.sql.gz
```

Инициализиурем базу:
```
zcat /usr/share/doc/zabbix-server-pgsql/create.sql.gz | sudo -u zabbix psql glaber
```

## Настройка сервера

Прописываем пользователя и пароль к БД в файле `/etc/zabbix/zabbix_server.conf` (пользователь `zabbix`, пароль тот, который был задан)

Устанвливаем в конфигурации флаг совместимости со старыми версиями БД: 
```
AllowUnsupportedDBVersions=1
```
##	Запуск сервера zabbix_server: 
```
systemclt zabbix-server start
```
убеждаемся, что сервер запустился и работает ( наличие процессов zabbix_server, отсутсвие ошибок в `/var/log/zabbix/zabbix_server.log`)

## Устанавка и настройка фронтенда


## AstraLinux 1.7.5 и более поздние
Особенностей нет, ставится как на  любой Linux Debian-Based системе. Важно установить пакеты php и расширений php версии 7.4 или более новые. На момент написания этой 
статьи в репозитории AstraLinux существуют пакеты php версии 8.2, рекомендуется их и использовать.

## AstraLinux 1.7.3 
Так как в поставке astra идет сильно устаревшая версия php, запускаем приложение php в среде Docker. 

### Установка в контейнере Docker 
```
apt install docker.io
```
Стартуем и добавляем в автозагрузку docker 
```
systemctl start docker
systemctl enable docker
```
Импортируем и запускаем образ приложения php-fpm8 для Docker
```
wget https://glaber.io/repo/misc/glaber-php-fpm-docker.gz
gunzip -c ./glaber-php-fpm-docker.gz | docker load
```
Примечание: среда docker для Glaber была собрана на хост-системе AstraLinux, контейнер использует alpine linux и версию php 8.1

Запускаем контейнер на базе скачанного образа, указываем что контейнер должен автоматически перезапускаться и иметь общую с сервером сетевую систему (чтобы иметь возможность соединяться с БД по локальному адресу):

```
docker run -d --network="host" --restart unless-stopped -v usr/share/zabbix:/usr/share/zabbix glaber-php-fpm-docker
```

Устанавливаем пакет с  фронтендом  glaber 

```
wget https://glaber.io/repo/astra/pool/main/g/glaber/glaber-frontend-php_3.0.70-1%2Bastra1.7.3_all.deb
dpkg -i ./glaber-frontend-php_3.0.70-1+astra1.7.3_all.deb
```

## Установка nginx:

```
apt install nginx
systemctl start nginx
systemctl enble nginx
```
Скачиваем и устанавливаем конфигурацию glaber для nginx:

```
dpkg -i glaber-nginx-conf_3.0.70-1+astra1.7.3_all.deb
```

Настраиваем фронтенд:  указываем имя пользователя и пароль к базе, удаляем в nginx сайт по-умолчанию: rm /etc/nginx/sites-enalbled/default.

Копируем пример конфигурации фронтенда :

```
 cp /usr/share/zabbix/conf/zabbix.conf.php.example /usr/share/zabbix/conf/zabbix.conf.php
```

Редактируем файл конфигурации `/usr/share/zabbix/conf/zabbix.conf.php`:
     указываем типа базы `POSTGRESQL`, имя пользователя `zabbiх`, пароль, который был задан при создании пользователя zabbix, копируем шрифт для графиков в каталог с фронтендом, так как по умолчанию устанавливается симлинк. 
```
rm /usr/share/zabbix/assets/fonts/graphfont.ttf
сp /etc/alternatives/zabbix-frontend-font usr/share/zabbix/assets/fonts/graphfont.ttf
```

Перезапускаем nginx
```
systemctl restart nginx
```
## Установка ClickHouse:

Подключаем репозиторий

```
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 8919F6BD2B48D754
echo "deb https://packages.clickhouse.com/deb stable main" | sudo tee \
    /etc/apt/sources.list.d/clickhouse.list
apt-get  update
```

устанавливаем 

```
apt install clickhouse-server clickhouse-client
```

добавляем в автозагрузку и стартуем

```
systemctl enable clickhouse-server
systemctl start clickhouse-server
```

задаем пароль для пользователя default, запоминаем, запускаем клиент 

```
clickhouse-client
```
создаем базу данных таблицы и таблицы, запросы можно найти в проекте в databases/clickhouse

```
clickhouse-client --password --multiquery < history.sql
```

## Настройка кликхаус: 
[Аналогично по инструкции с ubuntu](ubuntu)

Настраиваем работу с clickhouse в конфигурации сервера в `/etc/zabbix/zabbix_server.conf`: В директиве `HistoryModule` для clickhouse прописываем логин и пароль 

Проверям: во фронте должна начать рисоваться история

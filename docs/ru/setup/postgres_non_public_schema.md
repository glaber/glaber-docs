# Работа и настройка специфичной схемы в postgresql, отличной от default

В случае, когда требуется настроить работу системы со схемой postgresql отличной от default, рекомендуем настраивать новую базу в следующей последовательности:

Идея настройки состоит в следующем - нужно создать пользователя, дать ему право создавать объекты внутри любой базы

Создаем пользователя:
```
CREATE ROLE glaber WITH PASSWORD  '12345' ;
```

создаем базу для пользователя.
```
CREATE DATABASE glaber;
```

Разрешаем подключаться к базе.
```
ALTER ROLE glaber WITH LOGIN; 
```



Подключаемся к базе данных привелигированным пользователем (обычно postrges)
```
psql -h 10.11.12.13 -W postgres -d postgres -W 
```
Вводим пароль.

Создаем пользователя:
```
CREATE ROLE glaber WITH PASSWORD  '12345' ;
```

создаем базу для пользователя.

```
CREATE DATABASE glaber;
```

проверяем схемы
```
\dn
```

Разрешаем подключаться к базе.
```
ALTER ROLE glaber WITH LOGIN; 
```

Даем пользователю glaber привелеги создания схемы на базе glaber
```
GRANT CREATE ON DATABASE glaber TO glaber; 
```

Подключаемся к базе с glaber пользователем для проверки связи 
```
psql -h 10.11.12.13 -W glaber -d glaber -W 
```

Создаем схему glaber
```
CREATE SCHEMA glaber;
```

Записываем структуры и первоначальные данные в базу
(Файлы структуры и данных для базы данных поставляются с дистрибутивом glaber, либо нужно ее сгерерировать из исходного кода)
```
 zcat /usr/share/doc/zabbix-server-pgsql/create.sql.gz | psql -h 10.11.12.13 -W glaber -d glaber -W  
```

проверяем наличие схемы и к какой схеме принадлежат таблицы. По умолчанию все создается со схемой как имя пользователя.
```
 \dn+
                          List of schemas
  Name  |  Owner   |  Access privileges   |      Description
--------+----------+----------------------+------------------------
 glaber | glaber   |                      |
 public | postgres | postgres=UC/postgres+| standard public schema
        |          | =UC/postgres         |
(2 rows)

 \dt+ items
                                  List of relations
 Schema | Name  | Type  | Owner  | Persistence | Access method | Size  | Description
--------+-------+-------+--------+-------------+---------------+-------+-------------
 glaber | items | table | glaber | permanent   | heap          | 13 MB |
(1 row)

```


Запускаем glaber-server и убеждаемся по логу что все работает
```
systemctl start glaber-server
tail -n50 /var/log/zabbix_server.log
```

# Backup and restore

All data in Glaber is stored in two different databases:
    - в MySQL or PostgreSQL - configuration and system objects, logs, events
    - in timeseries storage - collected metrics, in most cases there is ClickHouse used as such storage.

## Backup and restore SQL data

### Mysql/Mariadb
You can find more advanced information about backup and restore here: https://mariadb.com/kb/en/backup-and-restore-overview/.

#### Backup
Run command (change <db_user> & <db_pass> on your username and password, that have acces to glaber database)
```
mysqldump -u<db_user> -p<db_pass> -E --single-transaction glaber > glaber.dump;
```
This command will make backup of data from glaber database to the file glaber.dump. This file yoi will use in case when it will be neccesary to restore database.

#### Restore
First of all you need create an empty database. If you allready have database, you need delete it first.
If database you need to delete is in use and there are glaber server active process is running, you need to stop it before you will be able to delete old database.

Use next SQL commands to restore database
```
mysql> DROP DATABASE glaber;
mysql> CREATE DATABASE glaber;
```
After you created database, you need to grant all rights to user that will work with it. If you have separate user that work with database from frontend side - you need to grant all priveledges to that user too.

If there are no created user at all, you need to create it first.
```
mysql> CREATE USER '<db_user>'@'localhost' IDENTIFIED BY '<db_pass>';
```
And then you can grant him all priveledges.
```
mysql> GRANT ALL PRIVILEGES ON glaber.* TO '<db_user>'@'localhost';
```
After new database is created and all neccesary users have priveledges to work with it, you can uses ```mysql``` utility to restore your backup:

mysql -u<db_user> -p<db_pass> glaber < glaber.dump

### Postgres SQL
Подробно процедуры бекапа и восстановления описаны на https://www.postgresql.org/docs/current/backup.html

#### Бекап
Выполните команду (замените <db_user> и <db_pass> на имя кользователя и его пароль, имеющего доступ к БД glaber)
```
/usr/bin/pg_dump --dbname=postgresql://<db_user>:<db_pass>@localhost:5432/$db  > glaber.dump;
```
Эта команда сделает дамп базы данных glaber в файл glaber.dump, который нужно будет использовать в случае необходимости восстановить базу

#### Восстановление
Авторизуйтесь под пользователем postgres и запустите psql
```
sudo su postgres
psql
```
Для восстановления нужно сначала создать пустую базу. Если база уже сущесвует, то сначала ее необходимо удалить.
Используйте SQL команды
```
postgres=# DROP DATABASE glaber;
postgres=# CREATE DATABASE glaber;
```
После создания базы на нее также нужно выдать права пользователю, или пользователям, от имени которых будет работать сервер и фронтенд:
Если пользователь не был заведен заранее, то создать его и задать пароль:
```
postgres=# CREATE USER <db_user> WITH PASSWORD '<db_pass>';
```
И выдать пользователю права на базу
```
postgres=# GRANT ALL PRIVILEGES ON DATABASE glaber TO <db_user>;
```
Завершите работу с psql с помощью команды exit
После чего из под попльзователя postgres через утилиту psql залейте бекап в новую базу:
```
psql glaber < glaber.dump
```
## Бекап и восстановление ClickHouse
Glaber может работать с различными timeseries хранилищами. У каждог из них есть свои утилиты и методы выгрузки\бекапа данных. Clickhouse в настоящий момент - самая популярная и удобная система для Glaber.
Стоит учитывать, что объемы данных могут быть очень большими, поэтому зачастую нужно реализовывать миграцию данных по частям (по партициям, по таблицам)
Процедуры бекапа и восстановления в Clickhouse описаны на: https://clickhouse.com/docs/en/operations/backup/

#### Бекап
Настройте, куда сервер будет бекапить данные. Для этого нужно создать файл /etc/clickhouse-server/config.d/backup_disk.xml
с содержимым, указывающим место в файловой системе, куда будут попадать бекапы (в данном случае /backups/):
```
<clickhouse>
    <storage_configuration>
        <disks>
            <backups>
                <type>local</type>
                <path>/backups/</path>
            </backups>
        </disks>
    </storage_configuration>
    <backups>
        <allowed_disk>backups</allowed_disk>
        <allowed_path>/backups/</allowed_path>
    </backups>
</clickhouse>
```
Далее выполните бекап командой
```
BACKUP DATABASE glaber TO Disk('backups', 'glaber.zip')
```
#### Восстановление
Настройте путь для работы с бекапами аналогично на сервере, на котором будут восстанавливаться данные, скопируйте в этот путь файл с бекапом и восстановите базу предварительно удалив существующую в случае её наличия:

DROP DATABASE glaber
RESTORE DATABASE glaber FROM Disk('backups', 'glaber.zip')

# Перенаправление метрик

## Перенаправление метрик между узлами

### Идея и мотивация

1. Иметь возможность распределять метрики из входящего потока метрик по узлам и айтемам на основе самих метрик. 

    Актуально для любых пассивных способов получения, протоколов, которые присылают данные активно. Например, `syslog`, `netflow`, импорт метрик из внешних систем. 

2.  Иметь унифицированный механизм распределения метрик, не зависящий от протокола. 

    Реализацией адаптеров для протоколов занимаются `воркеры`. Метрики от воркеров приходят в формате JSON. Информация и состав полей жестко не определены для гибкости системы. Необходимо иметь возможность на уровне конфигурации распределять такой входящий траффик метрик по узлам, которым этот траффик относится.

    Пример: в системе заведены три узла для серверов (host1, host2, host3). Сервера присылают свои логи в формате syslog на сервер мониторинга Glaber. Необходимо сохранять логи, присылаемые каждым из серверов в метрику с именем  "logs" соответсвующего узла.


3. Помимо распределения траффика по узлам, иметь возможность подсчитывать суммарные показатели по всему траффику

###  Архитектура перенаправления метрик между узлами

Для реализации распределения траффика используется препроцессинг.

![Alt text](images/dispatching.png)

Для приема траффика может использоваться как стандратная служба trapper, так и адаптеры под протоколы - воркеры, преобразующие специфичные протоколы в текстовые JSON объекты

Пример данных:

![Alt text](images/domofon_logs.png)
Пример: логи, собранные по протоколу syslog с домофонной панели Sokol. Подчеркнуты идентифицирующие панель данные - поле proc_id содержит физический адрес панели (MAC адрес)

Для распределения данных по узлам в приходящих данных должны быть объекты, позволяющие найти узел, которому данные принадлежат, как правило это имя, сетевой, физический адрес устройства. 

Настройка распределения делается в правилах препроцессинга. 

Для перенаправления на другие узлы может быть использовано одно из правил: 
    
* перенаправление по имени узла (`Dispatch by Hostname from JSON`): поиск узла приемника осуществляется по имени узла
    
* перенаправление по IP адресу узла (`Dispatch by IP from JSON`): поиск узла приемника осуществляется по IP адресу на любом из интерфейсов узла.

В правиле указывается имя ключа объекта, содержащего в значении имя или IP адрес узла. Имя ключа может быть указано строкой или выражением  JSON path.

В случае, если в IP адресе также указан порт, он автоматически отбрасывается и игнорируется.

Вторым параметром в правилах перенаправления указывается имя ключа метрики на узле-приемнике. 

## Перенаправление метрик в рамках одного узла

### Идея и мотивация

JSON метрики, как правило, содержат более одной метрики и/или могут содержать различные метрики в разных сообщениях. 

Требуется их преобразовывать и записывать в различные метрики. 

Например, когда изменяется административный статус на интерфейсе, устройстве пришлет  SNMP Trap, в одном  SNMP трапе может прийти две метрики - административное состояние порта и операционное состояние порта. 

Это разные метрики, их очень удобно выделить в различные айтемы. Воркер SNMPTrap пришлет две разных метрики в таком случае. Одну - с новым административным статусом интерфейса, и вторую - с новым операционным статусом.

Для того, чтобы перенаправить метрику по содержимому в различные метрики, в данном случае может использоваться локальное перенаправление.

*Примечание: задача также могла бы быть решена с помощью зависимых метрик, однако при большом количестве метрик, например, на многопортовых устройствах, портов, и различных трапов и метрик, могут быть тысячи. Такое количество зависимых метрики будет сложным как в конфигурировании, так и "дорогим" с точки зрения затрат процессора на копирование и фильтрацию.*

### Архитектура "локального" перенаправления метрик

Имя метрики получается из значения указанного в правиле ключа во входящих данных. Значение пересылается в целевую метрику без изменений и предполагается использование препроцессинга для получения значения.

Существуют две разновидности правила - для поиска по четкому соответствию значения ключа айтема и значения и для поиска по регулярному выражению. 

Регулярные выражения могут быть полезны для агрегирования различных метрик в одну. Например, все логи по работе портов устройства помещать в одну метрику.
# Package repositories

## Supported Operating Systems
At the moment, there are packages for the following distributions:
Debian (Jessie, Stretch, Buster, Bullseye), Ubuntu (Bionic, Xenial, Disco),
Centos 8

## branches of repositories
Three branches of repositories exists:

### Release
The last release of Glaber, for production use.

The repositories are at [https://glaber.debuggingetup/repos3(https://glaber.io/ru/setup/repos)

### testing
Repositories of packages with new functions.The packages was tested and checked on test environment.
Not production-ready. Testing versions improve and are supported before getting into the stage Release.

The repositories are at [https://glaber.io/ru/setup/repo-testing.(https://glaber.io/en/setup/repo-testing)

### Experimental
For custom packages, for local testing, debugging.This repository does not have to be used and it can
contain outdated or unsupported and non-working packages. Sometimes, in the process of solving and debugging, the Glaber team may ask you to install packages from the repository.

The repositories are at [https://glaber.io/repo-experimental.(https://glaber.io/repo-experimental)




** Note: ** Instructions are given for the *Release* repository, if you are using *Testing* or *Experimental*, change the repository name in the URL, and the URL key addresses: https://glaber.io/repo/ into https: // glaber.Io/repo-testing/ or into https://glaber.io/repo-experimental/, respectively.


## Adding repositories to Ubuntu:


Complete commands with administrator rights:
~~~
APT-GET UPDATE && Apt-GET Install WGET GNUPG2 LSB-Release Apt-Transport-HTTPS -y
Wget --quiet-o -https://glaber.io/repo/key/repo.gpg |Apt -Key Add -
echo "Deb [arch = amd64] https://glaber.io/repo/ubuntu $ (lsb_release-sc) main"> /etc/sources.list.d/glaber.list
Apt-Get Update
~~~

## Adding repositories to Debian:
~~~
Apt-Get Update
APT-GET Install WGET GNUPG2 LSB-Release Apt-Transport-Https -y
Wget --quiet-o -https://glaber.io/repo/key/repo.gpg |Apt -Key Add -
echo "Deb [arch = amd64] https://glaber.io/repo/debian $ (lsb_release-sc) main"> /etc/sources.list.d/glaber.list
Apt-Get Update
~~~

## Adding repositories to SENTOS:
~~~
Echo "[glaber]
name = Glaber Official Repository
Baseurl = https: //glaber.io/repo/rhel/ \ $ Releasever/
Enabled = 1
gpgcheck = 0
repo_gpgcheck = 0 "> /etc/yum.repos.d/glaber.repo
~~~

## Naming of packages

Packages has *glaber* prefix instead of *zabbix*, for example
glaber-server-pgslq, glaber-proxy-mysql

Before installing packages, you need to remove the corresponding zabbix* packages, since the names of the executable files are used the same, for example, "zabbix_server".

Glaber packages are supplied with a server, proxy, frontend.Agents, Senders, and other auxiliary software should be used from the official Zabbix sources, they are completely compatible with Glaber.

For installation, use commands
In Ubuntu and Debian
~~~
apt-get install glaber-server-mysql
~~~

In centos
~~~
yum install glaber-server-pgsql
~~~

# Расширение RTC команд сервера

RTC - run-time-control комнады используются для управления и диагностики

Glaber поддерживает стандартный набор команд [Zabbix](https://www.zabbix.com/documentation/current/en/manual/concepts/server)

Плюс существуют дополнительные возможности:
## Получение статистики по загруженности синхронных поллеров `get_sync_poller_times`
### Описание
Чтобы понять, какой тип метрик, не снимаемых асинхронно, является "узким" горлышком в системе, и, потенциально, решить вопрос с этими метриками путем вынесения на отдельные прокси, можно получить статистику по таким метрикам: 

```
zabbix_server -R get_sync_poller_times
```
Результат: 
```
# src/zabbix_server/zabbix_server -R get_sync_poller_times
Item type 3 time spent 0.04%, runs 0% (15 total)
Item type 5 time spent 0.00%, runs 0% (34 total)
Item type 19 time spent 0.01%, runs 0% (3 total)
Item type 20 time spent 99.17%, runs 99% (9701 total)
Idle time 0.77%, total runs 9766
```
